$('#slider-top').owlCarousel({
	nav: false,
	dots: false,
	autoplay: false,
	autoplayTimeout: 5000,
	margin: 15,
	responsive: {
		0: {
			items: 1,
			loop: ($('#slider-top .item').length > 1) ? true : false,
		}
	}
});
$('#slide-product-sale').owlCarousel({
	nav: true,
	dots: false,
	autoplay: false,
	autoplayTimeout: 5000,
	margin: 5,
	navText:[`<i class="fal fa-angle-left"></i>`, `<i class="fal fa-angle-right"></i>`],
	responsive: {
		0: {
			items: 2,
			loop: ($('#slide-product-sale .item').length > 2) ? true : false,
		},
		540: {
			items: 2,
			loop: ($('#slide-product-sale .item').length > 2) ? true : false,
		},
		768: {
			items: 3,
			loop: ($('#slide-product-sale .item').length > 3) ? true : false,
		},
		1200: {
			items: 4,
			loop: ($('#slide-product-sale .item').length > 4) ? true : false,
		}
	}
});

// count down 
var countDownDate = new Date("Sep 5, 2023 15:37:25").getTime();

var x = setInterval(function() {

  var now = new Date().getTime();
  var distance = countDownDate - now;
    
  var days = Math.floor(distance / (1000 * 60 * 60 * 24));
  var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
  var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
  var seconds = Math.floor((distance % (1000 * 60)) / 1000);
    
  document.getElementById("days").innerHTML = days
  document.getElementById("hours").innerHTML = hours
  document.getElementById("minus").innerHTML = minutes
  document.getElementById("seconds").innerHTML = seconds
    
  if (distance < 0) {
    clearInterval(x);
    document.getElementById("timer-view-count").innerHTML = "EXPIRED";
  }
}, 1000);
// count down 


var swiperdg = new Swiper('.danhgia-slider', {
	centeredSlides: true,
	loop: true,
	autoplay: false,
	navigation: {
		nextEl: '.thump-danhgia .swiper-button-next',
		prevEl: '.thump-danhgia .swiper-button-prev',
	},
	breakpoints: {
		280: {
			slidesPerView: 1,
			spaceBetween: 0
		},
		640: {
			slidesPerView: 1,
			spaceBetween: 0
		},
		768: {
			slidesPerView: 1,
			spaceBetween: 0
		},
		992: {
			slidesPerView: 3,
			spaceBetween: 15
		},
		1024: {
			slidesPerView: 3,
			spaceBetween: 15
		},
		1199: {
			slidesPerView: 3,
			spaceBetween: 15
		}
	}
});


var swiperblog = new Swiper('.blog-swiper', {
	slidesPerView: 3,
	loop: false,
	grabCursor: true,
	spaceBetween: 30,
	roundLengths: true,
	slideToClickedSlide: false,
	autoplay: false,
	navigation: {
		nextEl: '.block-blog .section-next',
		prevEl: '.block-blog .section-prev',
	},
	breakpoints: {
		300: {
			slidesPerView: 1,
			spaceBetween: 20
		},
		500: {
			slidesPerView: 1,
			spaceBetween: 20
		},
		640: {
			slidesPerView: 1,
			spaceBetween: 20
		},
		768: {
			slidesPerView: 2,
			spaceBetween: 20
		},
		991: {
			slidesPerView: 3,
			spaceBetween: 20
		}
	}
});



// // fix header 
// var prevScrollpos = window.pageYOffset;
// window.onscroll = function () {
// 	var currentScrollPos = window.pageYOffset;
// 	if (prevScrollpos > currentScrollPos) {
// 		$("#header").css({
// 			"position": "sticky",
// 			"top": 0,
// 		});
// 		$("#header").addClass('active');
// 	} else {
// 		$("#header").css({
// 			"top": "-100px",
// 		});
// 		$("#header").removeClass('active');
// 	}
// 	prevScrollpos = currentScrollPos;
// }

// $(window).on('scroll', function () {
// 	if ($(window).scrollTop()) {
// 		$('#header:not(.header-project-detail-not-scroll)').addClass('active');
// 	} else {
// 		$('#header:not(.header-project-detail-not-scroll)').removeClass('active')
// 	};
// });

// function resizeImage() {
// 	let arrClass = [
// 		{ class: 'resize-image-top', number: (674 / 1440) },
// 		{ class: 'resize-video-youtube', number: (376 / 668) },
// 		{ class: 'resize-image-pro', number: (250 / 180) },
// 		{ class: 'resize-image-detail', number: (653 / 470) },
// 	];
// 	for (let i = 0; i < arrClass.length; i++) {
// 		if($("." + arrClass[i]['class']).length){
// 			let width = document.getElementsByClassName(arrClass[i]['class'])[0].offsetWidth;
// 			$("." + arrClass[i]['class']).css('height', width * arrClass[i]['number'] + 'px');
// 		}
// 	}
// }

// resizeImage();
// new ResizeObserver(() => {
// 	resizeImage();
	
// }).observe(document.body)

// $(document).ready(function(){
// 	$("header#header .bars").click(function () {
// 		$(this).toggleClass('active');
// 		$('body').toggleClass('overlay');
// 		$('header#header .box-header-left .layout-menu').toggleClass('active');
// 		$('header#header').toggleClass('active-menu');
// 		$('main#main').toggleClass('active-menu');
// 	});
// 	$(document).mouseup(function (e) {
// 		var container = $("header#header .box-header-left .layout-menu, header#header");
// 		if (!container.is(e.target) &&
// 			container.has(e.target).length === 0) {
// 				$('header#header .bars').removeClass('active');
// 				$('body').removeClass('overlay');
// 				$('header#header .box-header-left .layout-menu').removeClass('active');
// 				$('header#header').removeClass('active-menu');
// 				$('main#main').removeClass('active-menu');
// 		}
// 	});
// })

$(document).ready(function() {
	$(".nav-item .box-menu>svg").click(function(){
		$(this).siblings(".ul_content_right_1").slideToggle(200)
		$(this).parents(".box-menu").toggleClass("active")
		$(this).toggleClass("active")
	})

	$(".header-menu .title_menu").click(function(){
		$(".box-catelory .header-mid").slideToggle(300)
		$('body').toggleClass('overlay');
	})

	$(document).mouseup(function (e) {
		var container = $(".box-catelory");
		if (!container.is(e.target) &&
			container.has(e.target).length === 0) {
				$(".box-catelory .header-mid").slideUp(300)
				$('body').removeClass('overlay');
		}
	});
	$(".header-mid .header-nav .nav .down_icon").click(function(){
		$(this).siblings('.dropdown-menu').slideToggle(300)
		// $('body').toggleClass('overlay');
	})
})
window.onscroll = function() {myFunction()};

var menu = document.getElementById("mega-menu-main");
var sticky = menu.offsetTop;

function myFunction() {
  if (window.pageYOffset > sticky) {
    menu.classList.add("pos_active");
  } else {
    menu.classList.remove("pos_active");
  }
}
